/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.algo9;

import java.util.Scanner;

/**
 *
 * @author BankMMT
 */
// 5678    range = 4   for (int i=4-1 ; i>=0 ;i--)  <--- index of string = range-1 
// i = 3       index 3 value = "8"  use method changeStrToInt <--- 8
//             temNum = 10**((range-1) - indexValue) <--- temNum = (10**(3-3))*8 <--- (10**0)*8 <--- 1*8 = 8     return 8
//             sum = 8
// i = 2       index 2 value = "7"  use method changeStrToInt <--- 7
//             temNum = 10**((range-1) - indexValue) <--- temNum = (10**(3-2))*7 <--- (10**1)*7 <--- 10*7 = 70     return 70
//             sum = 8+70 = 78
// i = 1       index 1 value = "6"  use method changeStrToInt <--- 6
//             temNum = 10**((range-1) - indexValue) <--- temNum = (10**(3-1))*6 <--- (10**2)*6 <--- 100*6 = 600     return 600
//             sum = 78+600 = 678
// i = 0       index 0 value = "5"  use method changeStrToInt <--- 5
//             temNum = 10**((range-1) - indexValue) <--- temNum = (10**(3-0))*5 <--- (10**3)*5 <--- 1000*5 = 5000     return 5000
//             sum = 678+5000 = 5678
public class Algo09 {

    public static void main(String[] args) {
        Scanner pm = new Scanner(System.in);
        System.out.println("Enter String type number : ");
        String str = pm.next();
        int sum = 0;
        int temNum;
        int unit = 1;
        boolean checkFail = false;                                                     //temporary numbers

        //System.out.println("Length number is : " + str.length());
        for (int i = str.length() - 1; i >= 0; i--) {
            if (isNumber(changeStrToInt(str.charAt(i)))) {
                temNum = changeStrToInt(str.charAt(i));
                //System.out.println("tem num : " + temNum);
                if ((str.length() - 1) - i != 0) {
                    for (int j = 0; j < (str.length() - 1) - i; j++) {
                        unit *= 10;
                    }
                }
                //System.out.println("unit : " + unit);
                sum += unit * temNum;
                //System.out.println("sum " + i + " : " + sum);
                unit = 1;
            } else {
                checkFail = true;
                break;
            }
        }
        if (checkFail) {
            System.out.println("You insert wrong input");
        } else {
            System.out.println("Answer is : " + sum);
        }

    }

    public static boolean isNumber(int answer) {
        if (answer == 999) {
            return false;
        }
        return true;
    }

    public static int changeStrToInt(char str) {
        int answer;
        switch (str) {
            case '0':
                answer = 0;
                break;
            case '1':
                answer = 1;
                break;
            case '2':
                answer = 2;
                break;
            case '3':
                answer = 3;
                break;
            case '4':
                answer = 4;
                break;
            case '5':
                answer = 5;
                break;
            case '6':
                answer = 6;
                break;
            case '7':
                answer = 7;
                break;
            case '8':
                answer = 8;
                break;
            case '9':
                answer = 9;
                break;
            default:
                answer = 999;
                break;
        }
        return answer;
    }
}
